package javabase;
import java.util.Scanner;
public class Chapter_05_02 {

	public static void main(String[] args) {

        Scanner input=new Scanner(System.in);
        int year,month,day;
        System.out.print("请输入年：");
        year=input.nextInt();
        System.out.print("请输入月：");
        month=input.nextInt();
        System.out.print("请输入日：");
        day=input.nextInt();
        int t=0,n=0,daySum=0,sum,allSum;
        switch(month)
        {
        case 1:
        	daySum+=31;
        case 2:
        	if((year%4==0&&year%100!=0)||(year%400==0)) daySum+=29;
        	else daySum+=28;
        case 3:
        	daySum+=31;
        case 4:
        	daySum+=30;
        case 5:
        	daySum+=31;
        case 6:
        	daySum+=30;
        case 7:
        	daySum+=31;
        case 8:
        	daySum+=31;
        case 9:
        	daySum+=30;
        case 10:
        	daySum+=31;
        case 11:
        	daySum+=30;
        case 12:
        	daySum+=31;break;
        default:
        	System.out.println("您输入的月份有误！");	
        }
        if((year%4==0&&year%100!=0)||(year%400==0))
        	sum=366-daySum+day;
        else
        	sum=365-daySum+day;
        
        if(year>1900)
        {
        	for(int i=1900;i<year;i++)
        	{
        		if((year%4==0&&year%100!=0)||(year%400==0))
        			t++;
        		else
        			n++;
        	}
        	allSum=t*366+n*365+sum-1;
        }
        else if(year<1900)
        {
        	for(int i=year;i<1900;i++)
        	{
        		if((year%4==0&&year%100!=0)||(year%400==0))
        			t++;
        		else
        			n++;
        	}
        	allSum=t*366+n*365-sum+1;
        }
        else
        {
        	allSum=sum-1;
        }
        
		System.out.print(year+"年"+month+"月"+day+"日离1900年1月1日相距"+allSum+"天");

	}

}
